DROP database shop_user;
GO
CREATE DATABASE shop_user;
GO
use shop_user;
GO
create table [dbo].[address](
    AddressId INT IDENTITY(1, 1)NOT NULL,
    Work VARCHAR(255),
    Recidence VARCHAR(255),
    Phone VARCHAR(10),
    CreatedAt DATETIME2 DEFAULT(SYSDATETIME()) NOT NULL,
    CONSTRAINT [PK_AdressId] PRIMARY KEY CLUSTERED(
        AddressId ASC
    )
);
GO
CREATE TABLE [user](
    UserId INT IDENTITY(1,1) NOT NULL,
    AddressId INT NOT NULL,
    Age INT NOT NULL,
    FristName VARCHAR(255) NOT NULL, 
    LastName VARCHAR(255) NOT NULL,
    Email VARCHAR(255) UNIQUE NOT NULL,
    Password VARCHAR(500) NOT NULL,
    CreatedAt DATETIME2 DEFAULT(SYSDATETIME()) NOT NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED(
        UserId ASC
    )
);
GO
CREATE TABLE [product](
    ProductId INT IDENTITY(1,1) NOT NULL,
    Name VARCHAR(500) UNIQUE NOT NULL,
    Amount INT DEFAULT(0) NOT NULL,
    Price MONEY DEFAULT(0) NOT NULL,
    CreatedAt DATETIME2 DEFAULT(SYSDATETIME()) NOT NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED(
        ProductId ASC
    )
);
GO
CREATE TABLE [invoice](
    InvoiceId INT IDENTITY(1, 1) NOT NULL,
    UserId INT NOT NULL,
    TotalPrice FLOAT,
    Status INT DEFAULT(0),
    CreatedAt DATETIME2 DEFAULT(SYSDATETIME()) NOT NULL,
    CONSTRAINT [PK_Inovice] PRIMARY KEY CLUSTERED(
        InvoiceId ASC
    )
);
GO
CREATE TABLE [product_invoice](
    ProductId INT, 
    InvoiceId INT
);
GO
ALTER TABLE [user]
ADD CONSTRAINT FK_Address
FOREIGN KEY(AddressId) REFERENCES Address(AddressId);
GO
ALTER TABLE [product_invoice]
ADD CONSTRAINT FK_Product_Invoice
FOREIGN KEY(ProductId) REFERENCES product(ProductId),
FOREIGN KEY(InvoiceId) REFERENCES invoice(InvoiceId);
GO
ALTER TABLE [invoice]
ADD CONSTRAINT FK_User
FOREIGN KEY(UserId) REFERENCES [dbo].[user](UserId);
GO
-- Data test

INSERT INTO [dbo].[address](Work,Recidence, Phone)
    VALUES('Development', 'Guayaquil, Ecuador', 0993421844)
GO
INSERT INTO [dbo].[user](AddressId, LastName, FristName,Age,Email, Password)
    VALUES(1,'Angel', 'Sanches', 23, 'sangel@gmail.com', 'angel537')
GO

INSERT INTO [dbo].[product](Name, Amount, Price) VALUES
    ('Mouse', 8, 10.50),
    ('Keyboard', 20, 45.50),
    ('SmartTV', 10, 10),
    ('Charger', 60, 40),
    ('Mainboard', 30, 100.50),
    ('Processor', 200, 400.50);


-- Stored Produres
GO
-- show all productos
CREATE PROCEDURE [dbo].[Product_GetAll]
AS
    BEGIN
        SELECT * FROM [dbo].[product];
    END
GO

-- Update amount product 
CREATE PROCEDURE [dbo].[Product_UpdateAmount]
    @ProductId INT,
    @Mount INT
AS
    BEGIN
        UPDATE [dbo].[product]
        SET  [Amount] = [Amount] - @Mount
        WHERE [ProductId] = @ProductId;
    END
GO

-- Generate class from table database
declare @TableName sysname = 'invoice'
declare @Result varchar(max) = 'public class ' + @TableName + '
{'

select @Result = @Result + '
    public ' + ColumnType + NullableSign + ' ' + ColumnName + ' { get; set; }
'
from
(
    select 
        replace(col.name, ' ', '_') ColumnName,
        column_id ColumnId,
        case typ.name 
            when 'bigint' then 'long'
            when 'binary' then 'byte[]'
            when 'bit' then 'bool'
            when 'char' then 'string'
            when 'date' then 'DateTime'
            when 'datetime' then 'DateTime'
            when 'datetime2' then 'DateTime'
            when 'datetimeoffset' then 'DateTimeOffset'
            when 'decimal' then 'decimal'
            when 'float' then 'double'
            when 'image' then 'byte[]'
            when 'int' then 'int'
            when 'money' then 'decimal'
            when 'nchar' then 'string'
            when 'ntext' then 'string'
            when 'numeric' then 'decimal'
            when 'nvarchar' then 'string'
            when 'real' then 'float'
            when 'smalldatetime' then 'DateTime'
            when 'smallint' then 'short'
            when 'smallmoney' then 'decimal'
            when 'text' then 'string'
            when 'time' then 'TimeSpan'
            when 'timestamp' then 'long'
            when 'tinyint' then 'byte'
            when 'uniqueidentifier' then 'Guid'
            when 'varbinary' then 'byte[]'
            when 'varchar' then 'string'
            else 'UNKNOWN_' + typ.name
        end ColumnType,
        case 
            when col.is_nullable = 1 and typ.name in ('bigint', 'bit', 'date', 'datetime', 'datetime2', 'datetimeoffset', 'decimal', 'float', 'int', 'money', 'numeric', 'real', 'smalldatetime', 'smallint', 'smallmoney', 'time', 'tinyint', 'uniqueidentifier') 
            then '?' 
            else '' 
        end NullableSign
    from sys.columns col
        join sys.types typ on
            col.system_type_id = typ.system_type_id AND col.user_type_id = typ.user_type_id
    where object_id = object_id(@TableName)
) t
order by ColumnId

set @Result = @Result  + '
}'

print @Result