
using System.Data;
using apiweb_project.Models;
using apiweb_project.Querys;
using Dapper;

public class InvoiceService : IInvoiceService
{
    private BaseQuery _query;


    public InvoiceService(IConfiguration configuration)
    {
        this._query = new BaseQuery(configuration);
    }

    public Invoice Create(Invoice invoice)
    {

        using (IDbConnection db = this._query.GetConnection())
        {
            db.QueryAsync(
                "@ INSERT INTO [invoice](UserId, TotalPrice, Status) " +
                "VALUES(@UserId, @TotalPrice)",
                new
                {
                    invoice.UserId,
                    invoice.TotalPrice,
                }
                );

            foreach (Product product in invoice.Products)
            {
                db.QueryAsync(@"INSERT INTO [invoice](ProductId, InvoiceId) "+
                "VALUES(@ProductId, @InvoiceId)", 
                new
                {
                    product.ProductId,
                    invoice.InvoiceId
                });
            }

            return db.Query<Invoice>(@"SELECT * FROM [invoice]")
                .Where(i => i.InvoiceId == invoice.InvoiceId)
                .First();
        }
    }

    public IEnumerable<Invoice> GetAll()
    {
        List<Invoice> invoices = new List<Invoice>();

        using (IDbConnection db = this._query.GetConnection())
        {
            invoices = db.Query<Invoice>(@"SELECT * FROM [invoice]").ToList();
        }

        return invoices;

    }

    public IEnumerable<Invoice> GetByIdUser(int id)
    {
        List<Invoice> invoices = new List<Invoice>();

        using (IDbConnection db = this._query.GetConnection())
        {
            invoices = db.Query<Invoice>(@"SELECT * FROM [invoice]")
                .Where(i => i.UserId == id)
                .ToList();
        }

        return invoices;
    }
}

public interface IInvoiceService
{
    public IEnumerable<Invoice> GetAll();
    public IEnumerable<Invoice> GetByIdUser(int id);
    public Invoice Create(Invoice invoice);

}