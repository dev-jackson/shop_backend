namespace apiweb_project.Services;

using System.Data;
using apiweb_project.Models;
using apiweb_project.Querys;
using Dapper;

public class UserService : IUserService{

    private BaseQuery query;

    public UserService(IConfiguration configuration){
        query = new BaseQuery(configuration);
    }
    public List<User> GetAll(){
        
        List<User> users = new List<User>();

        using (IDbConnection db = this.query.GetConnection())
        {
            users = db.Query<User>(@"SELECT * FROM [user]").ToList();
            foreach(User user in users){
                user.Address = db.Query<Address>(@"SELECT * FROM [address]")
                    .Where(a => user.AddressId == a.AddressId).First();
            }
        }

        return users;
    }

    public User GetUserByEmailAndPassword(UserLogin user){
        User? currentUser;

        using(IDbConnection db = this.query.GetConnection()){
            currentUser = db.Query<User>(@"SELECT * FROM [user]")
                .Where(u => u.Email == user.Email)
                .Where(u => u.Password == user.Password).FirstOrDefault();

            if(currentUser != null){
                currentUser.Address = db.Query<Address>(@"SELECT * FROM [address]").First();
            }    
        }
        return currentUser;
    }

    public async Task<User> Create(User user){
        using(IDbConnection db = this.query.GetConnection()){
            await db.QueryAsync(@"INSERT INTO [address] "+
            "VALUES(@work, @recidence, @phone)", new {
                user.Address.Work,
                user.Address.Recidence,
                user.Address.Phone
            });
        }
        return user;
    }

    public void Update(User user){

    }
}

public interface IUserService{
    public List<User> GetAll();
    public User GetUserByEmailAndPassword(UserLogin user);
}