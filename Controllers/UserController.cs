using apiweb_project.Models;
using apiweb_project.Services;
using Microsoft.AspNetCore.Mvc;

namespace apiweb_project.Controllers;

[Route("api/[controller]")]
public class UserController : Controller
{
    private readonly IUserService _userService;
    private readonly IInvoiceService _invoiceService;

    public UserController(IUserService userService, IInvoiceService invoiceService)
    {
        this._userService = userService;
        this._invoiceService = invoiceService;
    }

    [HttpGet]
    public IActionResult users()
    {
        try
        {
            return Ok(this._userService.GetAll());
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return NotFound();
        }
    }

    [HttpPost]
    [Route("login")]
    public IActionResult Post([FromBody] UserLogin user)
    {
        try
        {
            var currentUser = this._userService.GetUserByEmailAndPassword(user);
            if (currentUser != null)
            {
                return Ok(currentUser);
            }
            return NotFound();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return NotFound();
        }
    }

    [HttpGet]
    [Route("invoices/{id}")]
    public IActionResult Get(int id){
        try{
            var invoices = this._invoiceService.GetByIdUser(id);
            if(invoices != null){
                return Ok(invoices);
            }
            return NotFound();
        }catch(Exception e){
            return NotFound(e);
        }
    }
}