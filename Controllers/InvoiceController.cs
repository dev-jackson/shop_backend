
using apiweb_project.Models;
using Microsoft.AspNetCore.Mvc;

[Route("api/[controller]")]
public class InvoiceController : Controller {

    private readonly IInvoiceService _invoiceService;

    public InvoiceController(IInvoiceService invoiceService){
        this._invoiceService = invoiceService;
    }


    [HttpPost]
    public IActionResult Post([FromBody] Invoice invoice){
        try{
            return Ok();
        }catch(Exception e){
            return NotFound(e.Message);
        }
    }
}