namespace apiweb_project.Models;

public class Address{
    public int AddressId { get; set; }
    public string Work { get; set; }
    public string Recidence { get; set; }
    public string Phone { get; set; }
    public DateTime? CreatedAt { get; set; }
}